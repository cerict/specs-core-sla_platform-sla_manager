package eu.specsproject.slaplatform.slamanager.util;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;

import eu.specsproject.slaplatform.slamanager.entities.Annotation;
import eu.specsproject.slaplatform.slamanager.entities.SLA;

public class Main {

	public static void main(String[] args) {
		EntityManagerFactory factory = Persistence.createEntityManagerFactory("mongodb");
		
		//STORE
		EntityManager em = factory.createEntityManager();
        em.getTransaction().begin();       
        SLA sla = new SLA();
        sla.setState("NEGOTIATING");
        sla.setSlaXML("SLA XML 1");
        sla.setCustomer("Pasquale");

        sla.addAnnotation(new Annotation("VIOLATION","Bah","12345"));
        sla.addAnnotation(new Annotation("VIOLATION","Bah","12335"));
        em.persist(sla);

        //SEARCH
        Query query = em.createQuery("Select sla FROM SLA sla, IN(sla.annotations) as annotation where annotation.key LIKE '%' OR annotation.key = null");
        List<SLA> slas = query.getResultList();
        
        System.out.println("Found slas:" + slas.size());
        
        for (int i = 0; i<slas.size(); i++){
        	System.out.println("CUSTOMER: "+slas.get(i).getCustomer());
        }
        em.close();

	}
	


}
