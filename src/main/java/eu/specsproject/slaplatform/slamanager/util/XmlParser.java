package eu.specsproject.slaplatform.slamanager.util;

import org.w3c.dom.*;
import org.xml.sax.SAXException;

import javax.xml.parsers.*;

import java.io.*;

public class XmlParser {

    public static String getUserFromSla(String slaDocument) throws ParserConfigurationException, UnsupportedEncodingException, SAXException, IOException{
        String customer = "DEFAULT_CUSTOMER";
        if(slaDocument != null && slaDocument.startsWith("<")){
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        try {
            DocumentBuilder builder = factory.newDocumentBuilder();
            StringBuilder xmlStringBuilder = new StringBuilder();
            xmlStringBuilder.append(slaDocument);
            ByteArrayInputStream input =  new ByteArrayInputStream(xmlStringBuilder.toString().getBytes("UTF-8"));
            Document doc = builder.parse(input);
            NodeList nodeList = doc.getElementsByTagName("wsag:AgreementInitiator");
            if(nodeList.getLength() > 0){
                customer = nodeList.item(0).getTextContent();
            }
            System.out.println("CUSTOMER NAME: "+customer);
        } catch (ParserConfigurationException e) {
        	throw e;
        } catch (UnsupportedEncodingException e) {
        	throw e;
        } catch (SAXException e) {
        	throw e;
        } catch (IOException e) {
        	throw e;
        }
        }

        return customer;
    }
}
