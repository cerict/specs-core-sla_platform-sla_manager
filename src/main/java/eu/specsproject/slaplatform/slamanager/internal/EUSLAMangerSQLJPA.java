/*
Copyright 2015 SPECS Project - CeRICT

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

@author  Massimiliano Rak massimilinao.rak@unina2.it
@author  Valentina Casola casolav@unina.it
 */

package eu.specsproject.slaplatform.slamanager.internal;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import eu.specsproject.slaplatform.slamanager.entities.Lock;
import eu.specsproject.slaplatform.slamanager.entities.SLA;

/**
 * JPA SQL implementation.
 * This code is not intended to be part of the public API.
 * 
 * @author Mauro Turtur SPECS - CeRICT
 *
 */
public final class EUSLAMangerSQLJPA extends EUSLAMangerAbstractImpl {

	private EntityManagerFactory emFactory;

	public EUSLAMangerSQLJPA (EntityManagerFactory factory){
		emFactory = factory;
	}


	@Override
	List<SLA> persistenceSearch(String state, String customer, int start, int stop, String annotationKey, String annotationValue){

		EntityManager em =  emFactory.createEntityManager();
		List<SLA> slas = new ArrayList<SLA>();

		Query query = em.createQuery("Select sla FROM SLA sla, IN(sla.annotations) as annotation"
				+ " where "+getLikeClause(annotationKey, "annotation.key")+""
				+ " AND "+getLikeClause(annotationValue, "annotation.value")+""
				+ " AND "+getLikeClause(state, "sla.state")+""
				+ " AND "+getLikeClause(customer, "sla.customer"));

		if(stop != -1){
			slas = query.setFirstResult(start).setMaxResults(stop-start).getResultList();
		}else{
			slas = query.setFirstResult(start).getResultList();
		}			

		em.close();
		return slas;
	}

	private String getLikeClause(String value, String columnName){
		if(value == null){
			return "("+columnName+" LIKE '%' OR "+columnName+" = null)";
		}
		return "("+columnName+" = '"+value+"')";
	}

	@Override
	public void persistenceCreate(SLA sla) {
		EntityManager em =  emFactory.createEntityManager();
		EntityTransaction t = em.getTransaction();
		t.begin();
		em.persist(sla);
		t.commit();
		em.close();
	}

	@Override
	public
	SLA persistenceGetByID(String id) {
		if (id==null)
			throw new IllegalArgumentException("SLA identifier cannot be null");
		String iD = id;
		SLA sla = emFactory.createEntityManager().find(SLA.class,iD);

		if (sla==null)
			throw new IllegalArgumentException("SLA identifier is not valid");

		return sla;
	}

	@Override
	public String persistenceGetCustomerByID(String id) {
		if (id==null)
			throw new IllegalArgumentException("SLA identifier cannot be null");
		String iD = id;
		SLA sla = emFactory.createEntityManager().find(SLA.class,iD);

		if (sla==null)
			throw new IllegalArgumentException("SLA identifier is not valid");

		return sla.getCustomer().toString();
	}

	@Override
	void persistenceUpdate(SLA sla) {
		EntityManager em =  emFactory.createEntityManager();
		EntityTransaction t = em.getTransaction();
		t.begin();
		sla.setUpdated(new Date());
		em.merge(sla);
		t.commit();
		em.close();
	}

	@Override
	void persistenceRemoveSLAByID(String id) {
		if (id==null)
			throw new IllegalArgumentException("SLA Identifier cannot be null");
		String iD = id;
		EntityManager em = emFactory.createEntityManager();
		SLA sla = em.find(SLA.class,iD);

		if (sla==null)
			throw new IllegalArgumentException("SLA Identifier is not valid");

		em.getTransaction().begin();
		em.remove(sla);
		em.getTransaction().commit();

	}


	@Override
	Lock persistenceSetLock(String id) {

		Lock lock = new Lock();
		EntityManager em =  emFactory.createEntityManager();
		EntityTransaction t = em.getTransaction();
		t.begin();
		SLA sla = em.find(SLA.class, id);
		if(sla.getLock()==null){
			sla.setLock(lock);
			sla.setUpdated(new Date());
		} else 
			lock=null;
		t.commit();
		em.close();

		return lock;
	}


	@Override
	public
	boolean persistenceReleaseLock(String id, Lock lock) {
		EntityManager em =  emFactory.createEntityManager();
		EntityTransaction t = em.getTransaction();
		boolean ok=false;

		t.begin();
		SLA sla = em.find(SLA.class, id);

		if(lock.equals(sla.getLock())){
			sla.setLock(null);
			ok=true;
		}

		t.commit();
		em.close();

		return ok;
	}



}
