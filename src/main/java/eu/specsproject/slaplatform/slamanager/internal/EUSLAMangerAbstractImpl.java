/*
Copyright 2015 SPECS Project - CeRICT

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

@author  Massimiliano Rak massimilinao.rak@unina2.it
@author  Valentina Casola casolav@unina.it
 */

package eu.specsproject.slaplatform.slamanager.internal;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.ParserConfigurationException;

import org.xml.sax.SAXException;

import eu.specsproject.slaplatform.slamanager.SLAManager;
import eu.specsproject.slaplatform.slamanager.entities.Annotation;
import eu.specsproject.slaplatform.slamanager.entities.Lock;
import eu.specsproject.slaplatform.slamanager.entities.SLA;
import eu.specsproject.slaplatform.slamanager.entities.SLASTATEenum;
import eu.specsproject.slaplatform.slamanager.util.Pair;
import eu.specsproject.slaplatform.slamanager.util.XmlParser;


/**
 * Internal implementation of {@link SLAManager} interface.
 *  This code is not intended to be part of the public API.
 *  
 * @author Mauro Turtur SPECS - CeRICT
 *
 */
abstract class EUSLAMangerAbstractImpl implements SLAManager{

	/**
	 * Check SLA state against a vector of valid ones.
	 * If actual state is not found in constraint argument an  {@link IllegalStateException} will be thrown.
	 * 
	 * @param actualState - actual SLA state, not NULL
	 * @param constraints - SLA state vector
	 * @throws IllegalStateException in case actualState is not on constraints vector.
	 */
	private void checkState (String actualState, ArrayList<String> constraints ){

		if(constraints.contains(actualState)){
			return;
		}
		StringBuilder sb = new StringBuilder();
		for(String constr : constraints){
			sb.append(constr+"\n");
		}
		throw new IllegalStateException("Cannot perform requested operation in" + actualState +" SLA state, it not is present in: "+sb.toString());
	}

	/**
	 * The method changes the SLA state
	 * @param sla
	 * @param newState
	 * @param constraints
	 * @throws IllegalStateException
	 */
	synchronized private void changeState(SLA sla, String newState, ArrayList<String> constraints){
		checkState(sla.getState(), constraints);
		sla.setState(newState);
		persistenceUpdate(sla);
	}


	/**
	 * 
	 * The method implementation must ensure that the identifier is both not null and valid. 
	 * Otherwise, an {@link IllegalArgumentException} has to be thrown. 
	 * 
	 * @param id - SLA identifier, not null 
	 * @return the queried SLA from persistence
	 * @throws IllegalArgumentException in case id is null or SLA is not found
	 */
	abstract SLA  persistenceGetByID(String id);

	abstract String persistenceGetCustomerByID(String id);

	/**
	 * Create a SLA in persistence.
	 * 
	 * @param sla - the SLA to persist, not null
	 */
	abstract void persistenceCreate(SLA sla);

	/**
	 * update a SLA in persistence.
	 * 
	 * @param sla  - the SLA to update, not null
	 */
	abstract void persistenceUpdate(SLA sla);

	/**
	 * Search the SLAs in persistence.
	 * 
	 */
	abstract List<SLA> persistenceSearch(String state, String customer, int start, int end, String annotationKey, String annotationValue);

	/**
	 * Register a lock on a SLA in persistence.
	 * 
	 * @param id  - the SLA identifier to lock, not null
	 */
	abstract Lock persistenceSetLock(String id);

	/**
	 * Release a lock on a SLA in persistence.
	 * 
	 * @param id  - the SLA identifier to unlock, not null
	 * @param lock  - the Lock previously registered on the SLA, not null
	 * @return a boolean that specifies if the operation is executed correctly
	 */
	abstract boolean persistenceReleaseLock(String id, Lock lock);

	abstract void persistenceRemoveSLAByID(String id); 

	
	@Override
	public String create(String sla) throws ParserConfigurationException, SAXException, IOException{

		if (sla==null)
			throw new IllegalArgumentException("The SLA document cannot be null");

		//create SLA
		SLA internalSLA = new SLA();
		internalSLA.setState(SLASTATEenum.PENDING.toString());
		internalSLA.setSlaXML(sla);
		
		String customer = "DEFAULT_CUSTOMER";
		try {
			customer = XmlParser.getUserFromSla(sla);
		} catch (ParserConfigurationException | SAXException | IOException e) {
			throw e;
		}
		
		internalSLA.setCustomer(customer);
		persistenceCreate(internalSLA);
		return new  String(String.valueOf(internalSLA.getId()));

		//put SLA in negotiating state
		//        internalSLA.setState(SLASTATEenum.NEGOTIATING.toString());
		//        persistenceUpdate(internalSLA);
		//        System.out.println("Id created SLA: "+internalSLA.getId());
		//        return new  String(String.valueOf(internalSLA.getId()));
	}


	@Override
	public SLA retrieve(String id) {
		return  persistenceGetByID(id); 
	}

	@Override
	public String retrieveCustomerFromSla(String id){

		return persistenceGetCustomerByID(id);
	}

	@Override
	public Pair<String,Lock> retrieveAndLock(String id) {

		SLA sla = persistenceGetByID(id);
		ArrayList<String> constraints = new ArrayList<String>();
		constraints.add(SLASTATEenum.PENDING.toString());
		constraints.add(SLASTATEenum.NEGOTIATING.toString());
		constraints.add(SLASTATEenum.RENEGOTIATING.toString());

		checkState(sla.getState(), constraints);

		Lock lock = persistenceSetLock(id);
		return new Pair<String,Lock>(sla.getSlaXML(),lock);    
	}

	@Override
	public void remove(String id) {
		persistenceRemoveSLAByID(id); 
	}


	@Override
	public void update(String id, String sla, Lock lock)  throws ParserConfigurationException, SAXException, IOException{ 
		SLA intsla = persistenceGetByID(id);
		ArrayList<String> constraints = new ArrayList<String>();
		constraints.add(SLASTATEenum.PENDING.toString());
		constraints.add(SLASTATEenum.NEGOTIATING.toString());
		constraints.add(SLASTATEenum.RENEGOTIATING.toString());

		checkState(intsla.getState(), constraints);

		if (sla==null)
			throw new IllegalArgumentException("SLA cannot be null");

		intsla.setSlaXML(sla);
		
		String customer = "DEFAULT_CUSTOMER";
		try {
			customer = XmlParser.getUserFromSla(sla);
		} catch (ParserConfigurationException | SAXException | IOException e) {
			throw e;
		}
		intsla.setCustomer(customer);
		persistenceUpdate(intsla);


	}

	@Override
	synchronized public void userSign(String id, String sla, Lock lock)  throws ParserConfigurationException, SAXException, IOException{     

		SLA intsla = persistenceGetByID(id);
		ArrayList<String> constraints = new ArrayList<String>();
		constraints.add(SLASTATEenum.PENDING.toString());

		checkState(intsla.getState(), constraints);      
		if (sla!= null) {
			intsla.setSlaXML(sla);
			
			String customer = "DEFAULT_CUSTOMER";
			try {
				customer = XmlParser.getUserFromSla(sla);
			} catch (ParserConfigurationException | SAXException | IOException e) {
				throw e;
			}
			
			intsla.setCustomer(customer);
		}
		intsla.setState(SLASTATEenum.NEGOTIATING.toString());
		persistenceUpdate(intsla);

	}

	@Override
	synchronized public void sign(String id, String sla, Lock lock)  throws ParserConfigurationException, SAXException, IOException{     

		SLA intsla = persistenceGetByID(id);
		ArrayList<String> constraints = new ArrayList<String>();
		constraints.add(SLASTATEenum.PENDING.toString());
		constraints.add(SLASTATEenum.NEGOTIATING.toString());
		constraints.add(SLASTATEenum.RENEGOTIATING.toString());

		checkState(intsla.getState(), constraints);      
		if (sla!= null) {
			intsla.setSlaXML(sla);
			
			String customer = "DEFAULT_CUSTOMER";
			try {
				customer = XmlParser.getUserFromSla(sla);
			} catch (ParserConfigurationException | SAXException | IOException e) {
				throw e;
			}
			intsla.setCustomer(customer);
		}
		intsla.setState(SLASTATEenum.SIGNED.toString());
		persistenceUpdate(intsla);

	}

	@Override
	public void observe(String id) {
		ArrayList<String> constraints = new ArrayList<String>();
		constraints.add(SLASTATEenum.SIGNED.toString());
		constraints.add(SLASTATEenum.REMEDIATING.toString());
		constraints.add(SLASTATEenum.IMPLEMENTING.toString());
		constraints.add(SLASTATEenum.RENEGOTIATING.toString());

		changeState(persistenceGetByID(id), SLASTATEenum.OBSERVED.toString(), constraints);        
	}

	@Override
	public void implement(String id){
		ArrayList<String> constraints = new ArrayList<String>();
		constraints.add(SLASTATEenum.SIGNED.toString());

		changeState(persistenceGetByID(id), SLASTATEenum.IMPLEMENTING.toString(), constraints); 
	}


	@Override
	public void terminate(String id) {

		SLA sla = persistenceGetByID(id);
		ArrayList<String> constraints = new ArrayList<String>();
		constraints.add(SLASTATEenum.NEGOTIATING.toString());
		constraints.add(SLASTATEenum.OBSERVED.toString());
		constraints.add(SLASTATEenum.PROACTIVE_REDRESSING.toString());
		constraints.add(SLASTATEenum.RENEGOTIATING.toString());
		constraints.add(SLASTATEenum.REMEDIATING.toString());


		changeState(sla, SLASTATEenum.TERMINATING.toString(), constraints);    

		//change state

		sla.setState(SLASTATEenum.TERMINATED.toString());
		persistenceUpdate(sla);
	}

	@Override
	public void complete(String id) {
		ArrayList<String> constraints = new ArrayList<String>();
		constraints.add(SLASTATEenum.OBSERVED.toString());

		changeState(persistenceGetByID(id), SLASTATEenum.COMPLETED.toString(), constraints);

	}


	@Override
	public void signalAlert(String id) {
		ArrayList<String> constraints = new ArrayList<String>();
		constraints.add(SLASTATEenum.OBSERVED.toString());

		changeState(persistenceGetByID(id), SLASTATEenum.ALERTED.toString(), constraints);

	}
	
	@Override
	public void signalViolation(String id) {
		ArrayList<String> constraints = new ArrayList<String>();
		constraints.add(SLASTATEenum.OBSERVED.toString());
		constraints.add(SLASTATEenum.ALERTED.toString());

		changeState(persistenceGetByID(id), SLASTATEenum.VIOLATED.toString(), constraints);

	}
	
	@Override
	public void reNegotiate(String id) {
		ArrayList<String> constraints = new ArrayList<String>();
		constraints.add(SLASTATEenum.OBSERVED.toString());

		changeState(persistenceGetByID(id), SLASTATEenum.RENEGOTIATING.toString(), constraints);

	}

	@Override
	public void remediate(String id) {
		ArrayList<String> constraints = new ArrayList<String>();
		constraints.add(SLASTATEenum.VIOLATED.toString());

		changeState(persistenceGetByID(id), SLASTATEenum.REMEDIATING.toString(), constraints);

	}

	@Override
	public void redress(String id) {
		ArrayList<String> constraints = new ArrayList<String>();
		constraints.add(SLASTATEenum.ALERTED.toString());

		changeState(persistenceGetByID(id), SLASTATEenum.PROACTIVE_REDRESSING.toString(), constraints);

	}

	@Override
	public String getState(String id) {
		return persistenceGetByID(id).getState();
	}


	@Override
	public List<SLA> search(String state, String customer, int start, int stop, String annotationKey, String annotationValue) {

		return persistenceSearch(state, customer, start, stop, annotationKey, annotationValue);
	}



	@Override
	public void unlock(String id,Lock lock) {

	}

	//annotations
	@Override
	public void annotate(String id, Annotation annotation) {
		if (annotation==null)
			throw new IllegalArgumentException("Annotation cannot be null");

		SLA sla = persistenceGetByID(id);
		sla.addAnnotation(annotation);
		persistenceUpdate(sla);
	}

	@Override
	public List<Annotation> getAnnotations(String id) {
		SLA sla = persistenceGetByID(id);
		return sla.getAnnotations();  
	}

	@Override
	public void updateAnnotation(String id,String annId,Annotation ann){
	}




}
