/*
Copyright 2015 SPECS Project - CeRICT

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

@author  Massimiliano Rak massimilinao.rak@unina2.it
@author  Valentina Casola casolav@unina.it
*/

package eu.specsproject.slaplatform.slamanager.entities;

import java.io.Serializable;

public class Annotation implements Serializable  {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;
    private String id;
	private String key;
    private String value;
    private String timestamp;

    public Annotation(String key, String value, String timestamp){
        this.key = key;
        this.value = value;
        this.timestamp = timestamp;
    }
    
    public Annotation(){
        //Zero Arguments Constructor
    }
    

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public String getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(String timestamp) {
		this.timestamp = timestamp;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}
    
}
