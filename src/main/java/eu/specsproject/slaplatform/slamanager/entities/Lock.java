/*
Copyright 2015 SPECS Project - CeRICT

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

@author  Massimiliano Rak massimilinao.rak@unina2.it
@author  Valentina Casola casolav@unina.it
*/

package eu.specsproject.slaplatform.slamanager.entities;

import java.io.Serializable;
import java.util.Date;
import java.util.UUID;

import javax.persistence.Basic;
import javax.persistence.Embeddable;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Embeddable
public class Lock implements Serializable{
    
    /**
     * 
     */
    private static final long serialVersionUID = -5220086062106545930L;

    private UUID lockid;
    
    @Temporal(TemporalType.DATE)
    @Basic
    private Date timestamp;
    
    public Lock (){
        lockid=UUID.randomUUID();
        timestamp=new Date();
    }
    
     
    @Override
    public boolean equals(Object obj) {
        
         if (this == obj) 
             return true;
            if (obj == null || getClass() != obj.getClass()) 
                return false;

            Lock lock = (Lock) obj;

            if (!lockid.equals(lock.lockid)) 
                return false;
            
            return true;
    }
    
    @Override
    public int hashCode() {
        return 0;
    }


    public Date getTimestamp() {
        return timestamp;
    }

}
